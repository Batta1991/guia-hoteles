    $(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
            interval: 3000
        });

        $("#contacto").on('show.bs.modal' , function (e){
            console.log('el modal contacto se está mostrando')

            $('#contactoBtn1').removeClass("btn-outline-success");
            $('#contactoBtn1').addClass("btn-primary");
            $('#contactoBtn1').prop("disabled", true);

            $('#contactoBtn2').removeClass("btn-outline-success");
            $('#contactoBtn2').addClass("btn-primary");
            $('#contactoBtn2').prop("disabled", true);

            $('#contactoBtn3').removeClass("btn-outline-success");
            $('#contactoBtn3').addClass("btn-primary");
            $('#contactoBtn3').prop("disabled", true);

            $('#contactoBtn4').removeClass("btn-outline-success");
            $('#contactoBtn4').addClass("btn-primary");
            $('#contactoBtn4').prop("disabled", true);

            $('#contactoBtn5').removeClass("btn-outline-success");
            $('#contactoBtn5').addClass("btn-primary");
            $('#contactoBtn5').prop("disabled", true);

            $('#contactoBtn6').removeClass("btn-outline-success");
            $('#contactoBtn6').addClass("btn-primary");
            $('#contactoBtn6').prop("disabled", true);
        });
        $("#contacto").on('shown.bs.modal' , function (e){
            console.log('el modal contacto se mostró')
        });
        $("#contacto").on('hide.bs.modal' , function (e){
            console.log('el modal contacto se oculta')
        });
        $("#contacto").on('hidden.bs.modal' , function (e){
            console.log('el modal contacto se ocultó')
            $('#contactoBtn1').removeClass("btn-primary");
            $('#contactoBtn1').addClass("btn-outline-success");
            $('#contactoBtn1').prop("disabled", false);

            $('#contactoBtn2').removeClass("btn-primary");
            $('#contactoBtn2').addClass("btn-outline-success");
            $('#contactoBtn2').prop("disabled", false);

            $('#contactoBtn3').removeClass("btn-primary");
            $('#contactoBtn3').addClass("btn-outline-success");
            $('#contactoBtn3').prop("disabled", false);

            $('#contactoBtn4').removeClass("btn-primary");
            $('#contactoBtn4').addClass("btn-outline-success");
            $('#contactoBtn4').prop("disabled", false);

            $('#contactoBtn5').removeClass("btn-primary");
            $('#contactoBtn5').addClass("btn-outline-success");
            $('#contactoBtn5').prop("disabled", false);

            $('#contactoBtn6').removeClass("btn-primary");
            $('#contactoBtn6').addClass("btn-outline-success");
            $('#contactoBtn6').prop("disabled", false);
        });
    });